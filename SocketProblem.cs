﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SocketProblem
{

    class Block {
        public int blockStart;
        public int blockEnd;

        public Block() {
            this.blockStart = 0;
            this.blockEnd = 0;
        }

        public Block(int blockStart, int blockEnd) {
            this.blockStart = blockStart;
            this.blockEnd = blockEnd;
        }
    }

    class Task {
        public int pos;
        public int mach;
        public int val;

        public Task(int mpos,int mmach, int mval) {
            this.pos = mpos;
            this.mach = mmach;
            this.val = mval;
        }

    }


    class FlowShop
    {
        /*
         * 
         */
        private int n;

        /*
         * 
         */
        private int m;

        private int st;

        private int ms;

        /*
         * Tablica poprzedników maszynowych
         */
        private int[] _M;

        private int[] taskMachine;

        /*
         * Tablica następników maszynowych
         */
        private int[] M_;

        /*
         * Tablica poprzedników technologicznych
         */
        private int[] _T;

        /*
         * Tablica następników technologicznych
         */
        private int[] T_;

        /*
         * Tablica czasów wykonania
         */
        private int[] P;

        /*
         * Lista permutacji
         */
        private List<List<int>> pi;

        /*
         * Tablica liczby poprzendików
         */
        private int[] LP;

        /*
         * Tablica czasów zakończenia zadań
         */
        private int[] C;

        /*
         * Numer osttaniego zadania
         */
        private int lastTask;

        /*
         * lista zadań wyznaczająca ścieżkę krytyczną
         */
        private List<int> criticalPath;

        /*
         * lista bloków krytycznych
         */
        private List<Block> blocks;

        private int Cmax;


        public FlowShop(int n, int st, int ms, int[] P, List<List<int>> pi, int[] taskMachine)
        {
            this.n = n;
            this.st = st;
            this.ms = ms;
            
            this.P = P;
            this.pi = pi;

            this.taskMachine = taskMachine;
            this._T = new int[n*st + 1];
            this.T_ = new int[n*st + 1];
            this._M = new int[n*st + 1];
            this.M_ = new int[n*st + 1];
            this.LP = new int[n*st + 1];
            this.C = new int[n*st + 1];
            this.C[0] = 0;
            this.lastTask = 0;
            this.criticalPath = new List<int>();
            this.blocks = new List<Block>();
        }

        public bool prepareDataStructures()
        {

            /*
             * Generowanie tablicy poprzedników technologicznych i maszynowcyh
             * oraz tablicy liczby poprzedników
             */

            for (int i = 1; i <= this.n*this.st; i++)
            {
                if ((i - 1) % this.st != 0)
                {
                    this._T[i] = i - 1;
                    this.LP[i]++;
                }
                if ((i + 1) % this.st != 1)
                {
                    this.T_[i] = i + 1;
                }
                /*
                if (this.T_[i] > 0)
                {
                    this._T[T_[i]] = i;
                    this.LP[T_[i]]++;
                }
                 * /
                /*
                if (this.M_[i] > 0)
                {
                    this._M[M_[i]] = i;
                    this.LP[M_[i]]++;
                }
                */ 
            }
            
            for (int i = 0;i<this.ms*this.st;i++) {
                List<int> l = this.pi[i];
                for (int j = 1; j < l.Count; j++)
                {
                    this._M[l[j]] = l[j - 1];
                    this.M_[l[j-1]] = l[j];
                    this.LP[l[j]]++;
                }
            }
             

           
            Queue<int> q = new Queue<int>();
            
            for (int i = 1; i <= this.n*this.st; i++)
            {
                if (this.LP[i] == 0)
                {
                    q.Enqueue(i);

                }

            }

            
            int v;
            this.Cmax = 0;
            while (q.Count > 0)
            {
                v = q.Dequeue();
                this.C[v] = Math.Max(this.C[this._T[v]], this.C[this._M[v]]) + this.P[v];
                if (this.C[v] > this.Cmax)
                {
                    this.Cmax = this.C[v];
                    this.lastTask = v;
                }
                if (this.T_[v] != 0)
                {
                    if (--this.LP[this.T_[v]] == 0)
                    {
                        q.Enqueue(this.T_[v]);
                    }
                }
                if (this.M_[v] != 0)
                {
                    if (--this.LP[this.M_[v]] == 0)
                    {
                        q.Enqueue(this.M_[v]);
                    }
                }
            }
            //Console.WriteLine(this.Cmax);
            return true;
        }
        
        private void clear()
        {
            for (int i = 0; i <= this.n*this.st; i++)
            {
                this.C[i] = 0;
                this.LP[i] = 0;
            }
            this.criticalPath.Clear();
        }

        private void findCriticalPath()
        {
            int currentTask = this.lastTask;
            int blockStart, blockEnd;
            
            this.criticalPath.Add(currentTask);
            blockStart = currentTask;
            blockEnd = currentTask;
            do
            {

                // jeżeli poprzednik maszynowy ma taki sam czas zakończenia zadania jak czas rozpoczęcia obecnego zadania
                if (this.C[this._M[currentTask]] == this.C[currentTask] - this.P[currentTask])
                {
                    this.criticalPath.Add(this._M[currentTask]);
                    currentTask = this._M[currentTask];
                    blockStart = currentTask;
                }
                // w pp jeżeli poprzednik technologiczny ma taki sam czas zakończenia zadania jak czas rozpoczęcia obecnego zadania
                else if (this.C[this._T[currentTask]] == this.C[currentTask] - this.P[currentTask])
                {
                    this.criticalPath.Add(this._T[currentTask]);
                    currentTask = this._T[currentTask];
                    if (blockStart != blockEnd)
                    {
                        this.blocks.Add(new Block(blockStart, blockEnd));
                    }
                    blockEnd = blockStart = currentTask;
                }
                else
                {
                    break;
                }

            } while (this.C[currentTask] - this.P[currentTask] != 0);
            if (blockStart != blockEnd)
            {
                this.blocks.Add(new Block(blockStart, blockEnd));
            }
        }

        private void switchTasks(int task1, int task2) {

           

            List<int> l;

            for (int i = 0; i < this.m; i++)
            {
                l = this.pi[i];
                for (int j = 0; j < l.Count; j++)
                {
                    if (l[j] == task1)
                    {
                        l[j] = task2;
                    }
                    else if (l[j] == task2)
                    {
                        l[j] = task1;
                    }
                }
                this.pi[i] = l;
            }
        }

        private void moveTask(Task task1, Task task2) {

            List<int> l;
            

            l = this.pi[task1.mach-1];
            l.Remove(task1.val);

            l = this.pi[task2.mach - 1];
            l.Insert(task2.pos, task2.val);
            Console.WriteLine("x");
            
        }

        private void displayPi()
        {
            Console.WriteLine(" ");
            for (int i = 0; i < this.pi.Count; i++)
            {
                for (int j = 0; j < this.pi[i].Count; j++)
                {
                    Console.Write(this.pi[i][j] + " ");
                }
            }
            Console.Write("0 ");
        }

        public void solveSA()
        {
            float lambda = 0.999f;
            float T = 1f;

            this.clear();
            this.prepareDataStructures();
            int tmpCMax = this.Cmax;
            int rndTaskFrom, rndTaskTo, rndTaskFromMachine, rndTaskVal; ;
            int rndMchTo;
            int tmpCmax = 0;
            double prawd;
            int minCmax = 9999999;

            List<List<int>> tmpPi;
            
            Random rnd = new Random();
            
            while (T > 0.0000001)
            {
                T = T * lambda;
                
                int delta;
                tmpPi = new List<List<int>>(this.pi);

                this.clear();
                this.prepareDataStructures();
                tmpCmax = this.Cmax;
                if (this.Cmax < minCmax)
                {
                    minCmax = this.Cmax;
                    this.displayPi();
                }
                this.findCriticalPath();


                rndTaskFrom = rnd.Next(this.criticalPath.Count);
                rndTaskFromMachine = this.taskMachine[this.criticalPath[rndTaskFrom]];

                rndMchTo = ((this.taskMachine[this.criticalPath[rndTaskFrom]] -1) / this.ms)*this.ms + 1 + rnd.Next(this.ms);
                //rndMchTo = rndTaskFromMachine;
                rndTaskTo = rnd.Next(this.pi[rndMchTo - 1].Count);

                List<int> l;

                l = new List<int>(this.pi[rndTaskFromMachine-1]);
                l.Remove(this.criticalPath[rndTaskFrom]);
                this.pi[rndTaskFromMachine-1] = l;

                l = new List<int>(this.pi[rndMchTo-1]);
                l.Insert(rndTaskTo, this.criticalPath[rndTaskFrom]);
                this.taskMachine[this.criticalPath[rndTaskFrom]] = rndMchTo;
                this.pi[rndMchTo - 1] = l;
                rndTaskVal = this.criticalPath[rndTaskFrom];
                this.clear();
                this.prepareDataStructures();
                if (this.Cmax < minCmax)
                {
                    minCmax = this.Cmax;
                    this.displayPi();
                }
                //Console.WriteLine("temp: " + T + ", Cmax: " + this.Cmax);
                if (this.Cmax < tmpCmax)
                {
                    //Console.WriteLine("mniejsze");
                    
                }
                else
                {
                    delta = Math.Abs(this.Cmax - tmpCmax);
                    prawd = Math.Exp(-delta / T);
                    
                    
                    if (rnd.NextDouble() > prawd || delta == 0)
                    {
                        
                        //Console.WriteLine("prawdopodobienstwo nie przeszlo");
                        this.pi = new List<List<int>>(tmpPi);
                        this.taskMachine[rndTaskVal] = rndTaskFromMachine;
                    }
                    
                    else
                    {
                        //Console.WriteLine("STARE CMAX: " + tmpCmax + " NOWE CMAX: " + this.Cmax + " PRAWD: " + prawd);
                        
                        //Console.WriteLine("prawdopodobienstwo przeszlo");
                    }
                     
                    
                    //this.clear();
                    //this.prepareDataStructures();
                }

            }

            Console.WriteLine("\nWYNIK: " + minCmax);
        }

        

            
            /*
            for (int i = 1; i <= this.n; i++)
            {
                if (this.C[i] > this.C[this.lastTask])
                {
                    this.lastTask = i;
                }
                output = output + (this.C[i] - this.P[i]) + " " + this.C[i] + "\r\n";
            }
            output = output + "last Task: " + this.lastTask + "\r\n";
            output = output + C.Max().ToString();

            this.findCriticalPath();
            this.switchTasks(this.blocks[0].blockEnd, this._M[this.blocks[0].blockEnd]);
            this.clear();
            this.prepareDataStructures();

            output = "";

            for (int i = 1; i <= this.n; i++)
            {
                if (this.C[i] > this.C[this.lastTask])
                {
                    this.lastTask = i;
                }
                output = output + (this.C[i] - this.P[i]) + " " + this.C[i] + "\r\n";
            }
            output = output + "last Task: " + this.lastTask + "\r\n";
            output = output + C.Max().ToString();
            */ /*
            return output;
        }

        public string solveRandomized()
        {
            Random random = new Random();
            int tmpCMax = 99999999;
            int randomBlock, randomSide;
            this.prepareDataStructures();
            for (int i = 0; i < 100000; i++)
            {
                
                this.findCriticalPath();
                randomBlock = random.Next(this.blocks.Count);
                randomSide = random.Next(2);
                if (this.blocks.Count > 0)
                {
                    if (randomSide == 0)
                    {
                        this.switchTasks(this.blocks[randomBlock].blockStart, this.M_[this.blocks[randomBlock].blockStart]);
                    }
                    else
                    {
                        this.switchTasks(this.blocks[randomBlock].blockEnd, this._M[this.blocks[randomBlock].blockEnd]);
                    }
                }
                this.clear();
                
                this.prepareDataStructures();
                

                
                if (this.C.Max() < tmpCMax)
                {
                    tmpCMax = this.C.Max();
                }


            }
            String output;
            output = tmpCMax.ToString();
            return output;
        }


        public string solveTS()
        {
          
            this.prepareDataStructures();
            int tmpCMax = this.Cmax;
            int switchedTask1 = 0, switchedTask2 = 0;
            int task1, task2, task3, task4;
            String output = "";
            List<Block> blocks;
            Queue<Block> tabu = new Queue<Block>();
            DateTime start = DateTime.Now;
            for (int i = 0; i < 25000; i++)
            {
                this.findCriticalPath();
                blocks = new List<Block>(this.blocks);
                tmpCMax = 99999999;
                for (int j = 0; j < blocks.Count; j++)
                {
                    task1 = blocks[j].blockStart;
                    task2 = this.M_[blocks[j].blockStart];
                    task3 = blocks[j].blockEnd;
                    task4 = this._M[blocks[j].blockEnd];
                    this.switchTasks(task1, task2);
                    this.clear();
                    this.prepareDataStructures();
                    if (this.Cmax < tmpCMax && !this.searchPair(task1,task2,tabu))
                    {
                        tmpCMax = this.Cmax;
                        switchedTask1 = task1;
                        switchedTask2 = task2;
                    }

                    this.switchTasks(task2, task1);
                    this.clear();
                    this.prepareDataStructures();
                   
                    this.switchTasks(task3, task4);
                    this.clear();
                    this.prepareDataStructures();
                    if (this.Cmax < tmpCMax && !this.searchPair(task3, task4, tabu))
                    {
                        tmpCMax = this.Cmax;
                        switchedTask1 = task3;
                        switchedTask2 = task4;
                        
                    }

                    this.switchTasks(task4, task3);
                    this.clear();
                    this.prepareDataStructures();


                }

                // tabu
                if (tmpCMax != 99999999)
                {
                    Block pair = new Block(switchedTask1, switchedTask2);
                    if (tabu.Count >= 8)
                    {
                        tabu.Dequeue();
                    }
                    tabu.Enqueue(pair);
                    this.switchTasks(switchedTask1, switchedTask2);
                    this.clear();
                    this.prepareDataStructures();
                    output = "\r\n" + tmpCMax.ToString();
                }

            }

            
            /*
            for (int i = 1; i <= this.n; i++)
            {
                if (this.C[i] > this.C[this.lastTask])
                {
                    this.lastTask = i;
                }
                output = output + (this.C[i] - this.P[i]) + " " + this.C[i] + "\r\n";
            }
            output = output + "last Task: " + this.lastTask + "\r\n";
            output = output + C.Max().ToString();

            this.findCriticalPath();
            this.switchTasks(this.blocks[0].blockEnd, this._M[this.blocks[0].blockEnd]);
            this.clear();
            this.prepareDataStructures();

            output = "";

            for (int i = 1; i <= this.n; i++)
            {
                if (this.C[i] > this.C[this.lastTask])
                {
                    this.lastTask = i;
                }
                output = output + (this.C[i] - this.P[i]) + " " + this.C[i] + "\r\n";
            }
           
            DateTime stop = DateTime.Now;
            TimeSpan roznica = stop - start;
            output = output + "\r\n" + roznica.TotalMilliseconds + "ms";
            return output;
        }

        private Boolean searchPair(int task1, int task2, Queue<Block> q) {
            for (int i = 0; i < q.Count; i++)
            {
                if ((q.ElementAt(i).blockStart == task1 && q.ElementAt(i).blockEnd == task2) ||
                    (q.ElementAt(i).blockEnd == task1 && q.ElementAt(i).blockStart == task2)) {

                        return true;

                }
            }
            return false;
        }
        */
        
    }

   
}